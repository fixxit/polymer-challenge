# Introduction

# FixxIT Polymer Challenge

### Welcome fellow coder!
![Fellow Coder](https://media.giphy.com/media/OIEhvGRByVrHO/giphy.gif)

#### Introduction
---
You have chosen to undertake the FixxIT Polymer Challenge. This is a challenge and not a test! It is not about passing or failing, but rather about us giving you a clean canvas that you can use to demonstrate how passionate you are about coding. Thus, go forth **champion** and prove to us that you have what it takes to survive the every day life as a code wielding bug slayer!

Are you ready?

![Yes Sir](https://media.giphy.com/media/RavXJWRY3veEw/giphy.gif)

If that is your response, or something similar, we should probably get down to the details!

#### Before you start
---

As you might have noticed already, we have provided you with a "seed project" to get things going. This should give you a good point to start from without you having to setup things from scratch. *A lazy developer is a good developer.*

A few things that you should know before you start:
- Please read through the entire challenge before starting.
- You have only one week to finish the challenge.
- Please do frequent and well commented commits.
- Commenting your code is not mandatory, but will be appreciated.
- It does not have to be the prettiest.
- You are welcome to use a different Polymer seed project, but please provide the original link.
- Not all phases listed below are mandatory.
- It is not a race to try and finish all phases.
- Please tag the final commit of each phase completed.

#### Challenge description:
---

###### Background
So, I have this buddy called Frikkie. He who owns a second hand car garage that is called...wait for it...**Frikkie's cabs**, original right?

He would like you to create a website for his shop. These days Frikkie only sells his second hand cars online and could use a website of his own. With that said I guess we should do this in phases, because you know...agile and what not. So best you start at **phase 1** then!

###### Phase 1:
Lets start by creating a page that will display all of the cars that Frikkie has across all of his warehouses. Please sort the list according to *date_added* asc. In the project we added a file called *db.json* that contains all the second hand cars the Frikkie has available for sale.

###### Phase 2:
Now, allowing the user to click on any of the cars (that are licensed(true)) would make it a lot nicer. Once the user clicked on a car we could then also show a details page that displays things such as, the warehouse where it is stored and its location perhaps? It is up to you.

###### Phase 3:
Wow! Okay, we now have some good phunctionality! So lets take it one step further! Lets allow the user to add the car he is viewing to some sort of *shopping cart* so that he can easily checkout once he is done shopping. Oh, and maybe give him the total amount as well.

###### Phase 4:
Frikkie is also struggling with his admin. Can you maybe come up with an easy way that he can also see a list of the cars that are not licensed(false)?


#### Side notes:
---
- Kudos if you leave the project in working order.
- Kudos if you have unit test coverage.
- Kudos if you use es6.
- Kudos if you have no linting issues.
- Kudos if you use Redux to manage your shopping cart.

#### Getting started:
---
To get started, please fork this repository and create a branch with the same name as the key that was given to you via email. If you have not received a key yet, please contact <maruschka@fixx.it> or me <francois.vandermerwe@fixx.it>. Once you have finished the challenge or you have ran out of time, please put in a merge request to the develop branch of this repository.

After we had time to review your awesome solution we will get back to you.

If there is something that you do not know then we suggest that you take the opportunity and

![Google IT](https://media.giphy.com/media/XPmVwGTPC149a/giphy.gif)


##### Good luck and have fun!

# Project Table of Contents

- [How to start](#how-to-start)
- [Running tests](#running-tests)

# How to start
**Note** that this project requires node >=v6.9.0 or higher and npm >=3.10.3 but in order to be able to take advantage of the complete functionality we **strongly recommend node >=v8.x.x and npm >=5.x.x**.

### Setup

#### Prerequisites

Install [Polymer CLI](https://github.com/Polymer/polymer-cli) using
[npm](https://www.npmjs.com) (we assume you have pre-installed [node.js](https://nodejs.org)).

    npm install -g polymer-cli

Initialize project from template

    mkdir my-app
    cd my-app
    polymer init polymer-3-starter-kit


This command serves the app at `http://127.0.0.1:8081` and provides basic URL
routing for the app:

    npm start

#### Build

The `npm run build` command builds your Polymer application for production, using build configuration options provided by the command line or in your project's `polymer.json` file.

You can configure your `polymer.json` file to create multiple builds. This is necessary if you will be serving different builds optimized for different browsers. You can define your own named builds, or use presets. See the documentation on [building your project for production](https://www.polymer-project.org/3.0/toolbox/build-for-production) for more information.

The Polymer Starter Kit is configured to create three builds. These builds will be output to a subdirectory under the `build/` directory as follows:

```
build/
  es5-bundled/
  es6-bundled/
  esm-bundled/
```

* `es5-bundled` is a bundled, minified build with a service worker. ES6 code is compiled to ES5 for compatibility with older browsers.
* `es6-bundled` is a bundled, minified build with a service worker. ES6 code is served as-is. This build is for browsers that can handle ES6 code - see [building your project for production](https://www.polymer-project.org/3.0/toolbox/build-for-production#compiling) for a list.
* `esm-bundled` is a bundled, minified build with a service worker. It uses standard ES module import/export statements for browsers that support them.

Run `polymer help build` for the full list of available options and optimizations. Also, see the documentation on the [polymer.json specification](https://www.polymer-project.org/3.0/docs/tools/polymer-json) and [building your Polymer application for production](https://www.polymer-project.org/3.0/toolbox/build-for-production).

#### Preview the build

This command serves your app. Replace `build-folder-name` with the folder name of the build you want to serve.

    npm start build/build-folder-name/

# Running Tests

This command will run [Web Component Tester](https://github.com/Polymer/web-component-tester)
against the browsers currently installed on your machine:

    npm test

If running Windows you will need to set the following environment variables:

- LAUNCHPAD_BROWSERS
- LAUNCHPAD_CHROME

Read More here [daffl/launchpad](https://github.com/daffl/launchpad#environment-variables-impacting-local-browsers-detection)

